<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S1 A1</title>
</head>
<body>
	<h1>Full Address</h1>
	<p><?= getFullAddress("3F Caswynn Bldg., Timog Ave.", "Quezon City", "Metro Manila", "Philippines"); ?></p>
	<p><?= getFullAddress("3F Enzo Bldg., Buendia Ave.", "Makati City", "Metro Manila", "Philippines"); ?></p>

	<h1>Letter-Based Grading</h1>
	<p>87 is equivalent to <?= getLetterGrade(87); ?></p>

	<p>94 is equivalent to <?= getLetterGrade(94); ?></p>

	<p>74 is equivalent to <?= getLetterGrade(74); ?></p>
</body>
</html>