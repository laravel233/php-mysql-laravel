<?php

//[SECTION] Repetition Control Structures

//Repetition control structures or "loops" are used to execute code multiple times

//While Loop

//A while loop takes a single condition. For as long as the condition evaluates to true, the code inside the block will run.

function whileLoop(){
	$count = 5;

	while($count !== 0){
		echo $count . '</br>';
		$count--;
	}
}

//Do-While Loop

//A do-while loop works similarly to a while loop, but unlike a while loop, do-while loops guarantee that the code block will be executed at least once

function doWhileLoop(){
	$count = 10;

	do{
		echo $count . '</br>';
		$count--;
	}while($count > 0);
}

//For Loop
/*
	A for loop is a more flexible kind of loop consisting of three parts:
		-The initial value that will track the progression of the loop (the "counter")
		-The condition to be evaluated
		-The iteration method

	Syntax:
	for($counter; $condition; $iterationMethod){
		Code to execute
	}
*/

function forLoop(){
	for($count = 0; $count <= 10; $count++){
		echo $count . '</br>';
	}
}

//Continue and Break Statements

/*
	Continue is a keyword that allows to code to skip to the next iteration
	Break on the other hand is a keyword that ends the execution of the entire loop
*/

function modifiedForLoop(){
	for($count = 0; $count <= 20; $count++){
		if($count % 2 === 0){
			continue;
		}

		echo $count . '</br>';

		if($count > 10){
			break;
		}
	}	
}

//[SECTION] Array Manipulation

$studentNumbers = array('1923', '1924', '1925', '1926');
//$studentNumbers = ['1923', '1924', '1925', '1926'];

//Simple Array
$grades = [98.5, 94.3, 89.2, 90.1];

//Associative Arrays
$gradePeriods = [
	'firstGrading' => 98.5,
	'secondGrading' => 94.3,
	'thirdGrading' => 89.2,
	'fourthGrading' => 90.1
];

//Multi-Dimensional Arrays
$heroes = [
	['Iron Man', 'Thor', 'Hulk'],
	['Wolverine', 'Cyclops', 'Jean Grey'],
	['Batman', 'Superman', 'Wonder Woman']
];

//Multi-Dimensional Associative Arrays
$powers = [
	'regular' => ['Repulsor Blast', 'Rocket Punch'],
	'signature' => ['Unibeam']
];

$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

//Searching arrays:
function searchBrand($brand, $brandsArr){
	if(in_array($brand, $brandsArr)){
		return "$brand is in the array.";
	}else{
		return "$brand is NOT in the array.";
	}
}